/*
 *   CADaisyworld - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Parameters.java
 *
 * This class stores all of the parameters configured from the interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

public class Parameters {

    private final static int mImageSize = 400;
    private static final double mStephanConstant = 5.6696E-8;   // Stephan-Boltzmann constant

    private static int mGridSize = 20;                     // The size of the CA grid
    private static int mPixelSize = Parameters.getImageSize() / Parameters.getGridSize();

    private static String mLeftGridColour = "jet";         // The colour of the left CA
    private static String mRightGridColour = "greyscale";  // The colour of the right CA
    private static String mLeftGridData = "temperature";   // The data of the left CA
    private static String mRightGridData = "albedo";       // The data of the right CA
    private static boolean mPlotLeftCoverage = false;      // Plot daisy coverage on the left CA
    private static boolean mPlotRightCoverage = true;      // Plot daisy coverage on the right CA
    private static boolean mIsVonNeumann = false;          // von Neumann or Moore cell neighbourhood

    private static double mAlbedoGround = 0.5;        // The albedo of the bare ground
    private static double mDeathRate = 0.02;           // The rate of death of the daisies
    private static double mMutationRate = 0.02;        // The rate of mutation of the daisy albedo
    private static double mInitialTemperature = 22.5;  // The intial temperature of the grid sites
    private static int mDiffusionConstant = 500;      // The rate of heat conduction
    private static int mThermalCapacity = 2500;        // The capacity of the planet to hold heat

    private static double mMaximumInsolation = 3;
    private static double mInsolationStep = 0.001;

    // Getters
    public static int getImageSize() {
        return mImageSize;
    }

    public static double getStephanConstant() {
        return mStephanConstant;
    }

    public static int getPixelSize() {
        return mPixelSize;
    }

    public static void resetPixelSize() {
        mPixelSize = mImageSize / mGridSize;
    }

    public static int getGridSize() {
        return mGridSize;
    }

    public static String getLeftGridColour() {
        return mLeftGridColour;
    }

    public static String getRightGridColour() {
        return mRightGridColour;
    }

    public static String getLeftGridData() {
        return mLeftGridData;
    }

    public static String getRightGridData() {
        return mRightGridData;
    }

    public static boolean isPlotRightCoverage() {
        return mPlotRightCoverage;
    }

    public static boolean isPlotLeftCoverage() {
        return mPlotLeftCoverage;
    }

    public static boolean isVonNeumann() {
        return mIsVonNeumann;
    }

    public static double getAlbedoGround() {
        return mAlbedoGround;
    }

    public static double getDeathRate() {
        return mDeathRate;
    }

    public static double getMutationRate() {
        return mMutationRate;
    }

    public static double getInitialTemperature() {
        return mInitialTemperature;
    }

    public static int getDiffusionConstant() {
        return mDiffusionConstant;
    }

    public static int getThermalCapacity() {
        return mThermalCapacity;
    }

    public static double getMaximumInsolation() {
        return mMaximumInsolation;
    }

    public static double getInsolationStep() {
        return mInsolationStep;
    }

    public static void setGridSize( int gridSize ) {
        mGridSize = gridSize;
        resetPixelSize();
    }

    public static void setLeftGridColour( String leftGridColour ) {
        mLeftGridColour = leftGridColour;
    }

    public static void setRightGridColour( String rightGridColour ) {
        mRightGridColour = rightGridColour;
    }

    public static void setLeftGridData( String leftGridData ) {
        mLeftGridData = leftGridData;
    }

    public static void setRightGridData( String rightGridData ) {
        mRightGridData = rightGridData;
    }

    public static void setPlotRightCoverage( boolean plotRightCoverage ) {
        mPlotRightCoverage = plotRightCoverage;
    }

    public static void setPlotLeftCoverage( boolean plotLeftCoverage ) {
        mPlotLeftCoverage = plotLeftCoverage;
    }

    public static void setVonNeumann( boolean isVonNeumann ) {
        mIsVonNeumann = isVonNeumann;
    }

    public static void setAlbedoGround( double albedoGround ) {
        mAlbedoGround = albedoGround;
    }

    public static void setDeathRate( double deathRate ) {
        mDeathRate = deathRate;
    }

    public static void setMutationRate( double mutationRate ) {
        mMutationRate = mutationRate;
    }

    public static void setInitialTemperature( double initialTemperature ) {
        mInitialTemperature = initialTemperature;
    }

    public static void setDiffusionConstant( int diffusionConstant ) {
        mDiffusionConstant = diffusionConstant;
    }

    public static void setThermalCapacity( int thermalCapacity ) {
        mThermalCapacity = thermalCapacity;
    }

    public static void setMaximumInsolation( double maximumInsolation ) {
        mMaximumInsolation = maximumInsolation;
    }

    public static void setInsolationStep( double insolationStep ) {
        mInsolationStep = insolationStep;
    }

    public static void centreWindow( Window frame ) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = ( int )( ( dimension.getWidth() - frame.getWidth() ) / 2 );
        int y = ( int )( ( dimension.getHeight() - frame.getHeight() ) / 2 );
        frame.setLocation( x, y );
    }
}
