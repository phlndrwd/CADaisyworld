/*
 *   CADaisyworld - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MainThread.java
 *
 * <p>This is a two-dimensional Daisyworld example that attempts to replicate
 * as faithfully as possible the equations of von Bloh et al. (1997), references
 * to equations within the comments relate to this paper. The model variables
 * are passed at the time of creation via the constructor, with the exception of
 * the variable S, otherwise known as the insolation "control parameter" which
 * is stepped via a loop in the run() method.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package model;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.SwingUtilities;
import draw.DaisyDraw;
import ui.MainInterface;

/*
 * This class implements the runnable interface so that an object of this type
 * can be attached to a thread which allows the interface to remain active
 * while the loops and calculations are being run.
 */
public final class MainThread implements Runnable {

    MainInterface mParent;

    // Model tn arrays; for this time-step.
    private final double[][] mTemperature;     // Two-dimensional temperature array
    private final double[][] mAlbedo;          // Two-dimensional albedo array
    private final boolean[][] mCoverage;        // Two-dimensional coverage array

    // Model tn+1 arrays; for the next time-step
    private final double[][] mNewTemperature;  // New two-dimensional temperature array
    private final double[][] mNewAlbedo;       // New two-dimensional albedo array
    private final boolean[][] mNewCoverage;    // New two-dimensional coverage array

    // Model variables
    private int mHorizonalIndex;     // Horizontal index of the next neighbour cell
    private int mVerticalIndex;      // Vertical index of the next neighbour cell

    private final BufferedImage mRightBufferedImage; // Buffered off screen image to the right panel
    private final BufferedImage mLeftBufferedImage;  // Buffered off screen image to the left panel

    private final Graphics mRightBufferedGraphics;   // Graphics component of the right CA off screen image
    private final Graphics mLeftBufferedGraphics;    // Graphics component of the left CA off screen image

    private final Random mRandom = new Random();     // Random object used for random value generation in several methods

    public MainThread( MainInterface parent ) {

        mParent = parent;

        mRightBufferedImage = new BufferedImage( Parameters.getImageSize() + 1, Parameters.getImageSize() + 1, BufferedImage.TYPE_INT_RGB );
        mLeftBufferedImage = new BufferedImage( Parameters.getImageSize() + 1, Parameters.getImageSize() + 1, BufferedImage.TYPE_INT_RGB );

        mRightBufferedGraphics = mRightBufferedImage.getGraphics();
        mLeftBufferedGraphics = mLeftBufferedImage.getGraphics();

        // Size tn arrays
        mTemperature = new double[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        mAlbedo = new double[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        mCoverage = new boolean[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        // Size tn+1 arrays
        mNewTemperature = new double[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        mNewAlbedo = new double[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        mNewCoverage = new boolean[ Parameters.getGridSize() ][ Parameters.getGridSize() ];

        for( int i = 0; i < Parameters.getGridSize(); i++ ) {
            for( int j = 0; j < Parameters.getGridSize(); j++ ) {
                mCoverage[ i ][ j ] = true;
                mAlbedo[ i ][ j ] = mRandom.nextDouble();
                mTemperature[ i ][ j ] = Parameters.getInitialTemperature();
            }
        }
    }

    /*
     * The value true is generated with a probability of that put in as a
     * parameter.
     * @param probability: The given probability, in the range 0 to 1.
     * @return The boolean response; true or false.
     */
    private boolean coverageFunction( double probability ) {

        boolean outcome[] = new boolean[ 100 ];

        probability = probability * 100;
        int probPercent = ( int )probability;

        for( int i = 0; i < probPercent; i++ ) {
            outcome[ i ] = true;
        }
        for( int i = probPercent; i < 100; i++ ) {
            outcome[ i ] = false;
        }
        int ranIndex = mRandom.nextInt( 100 );

        return outcome[ ranIndex ];
    }

    /*
     * Represents the calculation of the first and second derivative from the
     * partial differential equation of von Bloh et al.'s equation (6). The
     * function takes horiztontal and vertical indexes of the cell concerned as
     * well as the maximum in order to retrieve the temperature of the
     * surrounding cells. If the cell is on an edge or in a corner the function
     * assumes the cells outside of the range would have the same temperature as
     * that being looked at; this represents total insulation around the
     * perimeter of the CA grid.
     * @param i The horizontal index of the cell being looked at.
     * @param j The vertical index of the cell being looked at.
     * @param vonNeumann von Neumann or Moore cell neighborhood.
     * @return The temperature gradient of the cell being looked at.
     */
    private double calcTempGradient( int i, int j, boolean vonNeumann ) {

        double tempGradient = 0;

        if( vonNeumann ) {
            if( i == 0 ) {
                tempGradient += mTemperature[ i ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j ];
            } else if( i == Parameters.getGridSize() - 1 ) {
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i ][ j ];
            } else {
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j ];
            }
            if( j == 0 ) {
                tempGradient += mTemperature[ i ][ j ];
                tempGradient += mTemperature[ i ][ j + 1 ];
            } else if( j == Parameters.getGridSize() - 1 ) {
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i ][ j ];
            } else {
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i ][ j + 1 ];
            }
            tempGradient -= mTemperature[ i ][ j ] * 4;
        } else {                    // Else, uses a Moore neighborhood
            if( ( i == 0 ) && ( j == 0 ) ) {
                tempGradient += mTemperature[ i ][ j ] * 5;
                tempGradient += mTemperature[ i ][ j + 1 ];
                tempGradient += mTemperature[ i + 1 ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j + 1 ];
            } else if( ( i == Parameters.getGridSize() - 1 )
                    && ( j == Parameters.getGridSize() - 1 ) ) {
                tempGradient += mTemperature[ i ][ j ] * 5;
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i - 1 ][ j - 1 ];
            } else if( ( i == 0 ) && ( j == Parameters.getGridSize() - 1 ) ) {
                tempGradient += mTemperature[ i ][ j ] * 5;
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i + 1 ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j - 1 ];
            } else if( ( i == Parameters.getGridSize() - 1 ) && ( j == 0 ) ) {
                tempGradient += mTemperature[ i ][ j ] * 5;
                tempGradient += mTemperature[ i ][ j + 1 ];
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i - 1 ][ j + 1 ];
            } else if( i == 0 ) {
                tempGradient += mTemperature[ i ][ j ] * 3;
                tempGradient += mTemperature[ i ][ j + 1 ];
                tempGradient += mTemperature[ i + 1 ][ j ];
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i + 1 ][ j + 1 ];
                tempGradient += mTemperature[ i + 1 ][ j - 1 ];
            } else if( i == Parameters.getGridSize() - 1 ) {
                tempGradient += mTemperature[ i ][ j ] * 3;
                tempGradient += mTemperature[ i ][ j + 1 ];
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i - 1 ][ j + 1 ];
                tempGradient += mTemperature[ i - 1 ][ j - 1 ];
            } else if( j == 0 ) {
                tempGradient += mTemperature[ i ][ j ] * 3;
                tempGradient += mTemperature[ i + 1 ][ j ];
                tempGradient += mTemperature[ i ][ j + 1 ];
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j + 1 ];
                tempGradient += mTemperature[ i - 1 ][ j + 1 ];
            } else if( j == Parameters.getGridSize() - 1 ) {
                tempGradient += mTemperature[ i ][ j ] * 3;
                tempGradient += mTemperature[ i + 1 ][ j ];
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j - 1 ];
                tempGradient += mTemperature[ i - 1 ][ j - 1 ];
            } else {
                tempGradient += mTemperature[ i ][ j + 1 ];
                tempGradient += mTemperature[ i + 1 ][ j ];
                tempGradient += mTemperature[ i ][ j - 1 ];
                tempGradient += mTemperature[ i - 1 ][ j ];
                tempGradient += mTemperature[ i + 1 ][ j + 1 ];
                tempGradient += mTemperature[ i + 1 ][ j - 1 ];
                tempGradient += mTemperature[ i - 1 ][ j + 1 ];
                tempGradient += mTemperature[ i - 1 ][ j - 1 ];
            }
            tempGradient -= mTemperature[ i ][ j ] * 8;
        }
        return tempGradient;
    }

    /*
     * Next neighbour cell selection function
     * @vonNeumann von Neumann or Moore cell neighborhood.
     * @i The horizontal index of the cell being looked at.
     * @j The vertical index of the cell being looked at.
     */
    private void selectNeighbour( boolean vonNeumann, int i, int j ) {
        if( vonNeumann ) {
            if( mRandom.nextBoolean() ) {
                do {
                    randomIIndex( i );
                } while( getHorizonalIndex() == i );
            } else {
                do {
                    randomJIndex( j );
                } while( getVerticalIndex() == j );
            }
        } else {
            do {
                randomIIndex( i );
                randomJIndex( j );
            } while( ( getHorizonalIndex() == i ) && ( getVerticalIndex() == j ) );
        }
    }

    /*
     * From von Bloh et al.'s equations (9) and (11), this function takes the
     * current horizontal index and returns a random one in order to select a
     * "next neighbour cell". The function is called in conjunction with the
     * equivalent for the vertical axis from within a do while loop to ensure
     * that a neighbour and not the current cell is selected.
     * @param i The current horizontal index.
     * @return The random horizontal index.
     */
    private void randomIIndex( int i ) {
        int index;

        if( i == 0 ) {
            index = mRandom.nextInt( 2 );
            i = i + index;
        } else if( i == Parameters.getGridSize() - 1 ) {
            index = mRandom.nextInt( 2 );
            i = i - index;
        } else {
            index = mRandom.nextInt( 3 ) - 1;
            i = i + index;
        }
        setHorizonalIndex( i );
    }

    /*
     * From von Bloh et al.'s equations (9) and (11), this function takes the
     * current vertical index and returns a random one in order to select a
     * "next neighbour cell". The function is called in conjunction with the
     * equivalent for the horizontal axis from within a do while loop to ensure
     * that a neighbour and not the current cell is selected.
     * @param j The current vertical index.
     * @return The random vertical index.
     */
    private void randomJIndex( int j ) {

        int index;

        if( j == 0 ) {
            index = mRandom.nextInt( 2 );
            j = j + index;
        } else if( j == Parameters.getGridSize() - 1 ) {
            index = mRandom.nextInt( 2 );
            j = j - index;
        } else {
            index = mRandom.nextInt( 3 ) - 1;
            j = j + index;
        }
        setVerticalIndex( j );
    }

    /*
     * From von Bloh et al.'s equation (2), the algorithm represents a parabolic
     * growth function that peaks at one for the optimum temperature for daisy
     * growth (22.5 deg C) and remains at zero for a temperature less than 5 deg
     * C or greater than 40 deg C.
     * @param temp The temperature from which the growth rate is calculated.
     * @return The growth rate at the given temperature.
     */
    private double growthFunction( double temperature ) {

        double growth = 0;

        // Daisies don't grow if temperature is less that 5C or greater than 40C
        if( ( temperature >= 5 ) && ( temperature <= 40 ) ) {

            // Calculate growth using a standard parabolic growth function
            growth = ( 4 / Math.pow( ( 40 - 5 ), 2 ) ) * ( temperature - 5 ) * ( 40 - temperature );
        }

        return growth;
    }

    /*
     * From von Bloh et al.'s equations (9), (12) and (13). The function takes
     * an albedo representing a particular daisy species and generates a random
     * mutation based on a distribution of the specified mutation rate.
     * @param albedo The albedo of the daisy to mutate.
     * @return The mutated albedo.
     */
    private double mutateAlbedo( double albedo ) {

        // Pick random value within a distribution +/- of mutation rate
        double albedoAddition = 2 * Parameters.getMutationRate() * mRandom.nextDouble() - Parameters.getMutationRate();

        // Alter albedo according to mutation
        albedo = albedo + albedoAddition;

        if( albedo < 0 ) {
            albedo = 0;
        }

        if( albedo > 1 ) {
            albedo = 1;
        }

        return albedo;
    }

    /*
     * Calculates new values of temperature, coverage and albedo based on the
     * current ones.
     * @param s The value of insolation.
     * @param i The horizontal index of the grid site to look at.
     * @param j The vertical index of the grid site to look at.
     */
    private void calcArrays( double s, int i, int j ) {
        double growth;
        // Equation (7)
        if( mCoverage[ i ][ j ] ) {           // Cell is covered
            mNewCoverage[ i ][ j ] = coverageFunction( 1 - Parameters.getDeathRate() );

            if( mNewCoverage[ i ][ j ] ) {    // If site in tn+1 is covered
                mNewAlbedo[ i ][ j ] = mAlbedo[ i ][ j ]; // Albedo stays the same
            } else {
                mNewAlbedo[ i ][ j ] = Parameters.getAlbedoGround(); // Else, albedo bare ground
            }
        } else {                        // Cell is uncovered

            selectNeighbour( Parameters.isVonNeumann(), i, j );  // Select random neighbour

            // Equation (9)
            if( mCoverage[ getHorizonalIndex() ][ getVerticalIndex() ] ) { // Daisy in the next neighbour cell

                // Equation (11); growth depends on the temp. of NNC (default)
                growth = growthFunction( mTemperature[ getHorizonalIndex() ][ getVerticalIndex() ] );

                // Equation (10); growth depends on temp. of the uncovered cell
                // growth = growthFunction(temperature[i][j]); // Not used
                // Calculate coverage in tn+1 based on growth at NNC
                mNewCoverage[ i ][ j ] = coverageFunction( growth );

                if( mNewCoverage[ i ][ j ] ) {    // If site in tn+1 is covered
                    // Mutate the albedo of next neighbour cell and set
                    mNewAlbedo[ i ][ j ] = mutateAlbedo( mAlbedo[ getHorizonalIndex() ][ getVerticalIndex() ] );
                } else {                    // Else, site in tn+1 is bare
                    // Set albedo to that of bare ground
                    mNewAlbedo[ i ][ j ] = Parameters.getAlbedoGround();
                }
                // Equation (8); no daisy in the next neighbour cell
            } else {
                mNewCoverage[ i ][ j ] = false;
                mNewAlbedo[ i ][ j ] = Parameters.getAlbedoGround();
            }
        }

        double temperatureChange = ( 1 / Parameters.getThermalCapacity() ) * ( Parameters.getDiffusionConstant() * calcTempGradient( i, j, Parameters.isVonNeumann() ) ) - ( Parameters.getStephanConstant() * Math.pow( mTemperature[ i ][ j ], 4 ) ) + ( s * ( 1 - mAlbedo[ i ][ j ] ) );

        // Equation 6; calculate new temperature using the temperature gradient
        mNewTemperature[ i ][ j ] = mTemperature[ i ][ j ] + temperatureChange;
    }

    /*
     * This is the main method from which all of the calculations and plotting
     * of the model are goverened. It is an implementation of the run() method
     * provided by the Runnable interface which enables this class to be run
     * as a thread, preventing the GUI from locking. When set up correctly with
     * a thread object this method is called when the thread's start() method
     * is executed.
     */
    @Override
    public void run() {
        DaisyDraw draw = new DaisyDraw();

        terminate:
        for( double s = 0; s < 2; s += 0.001 ) {           // Insolation loop
            for( int i = 0; i < Parameters.getGridSize(); i++ ) { // Horizontal loop
                for( int j = 0; j < Parameters.getGridSize(); j++ ) { // Vertical loop

                    calcArrays( s, i, j );

                    if( Parameters.getRightGridData().equalsIgnoreCase( "albedo" ) ) {
                        draw.drawGrid( mAlbedo[ i ][ j ], 1, Parameters.getRightGridColour(), Parameters.isPlotRightCoverage(), mCoverage[ i ][ j ], Parameters.getPixelSize(), i, j, mRightBufferedGraphics );
                    } else if( Parameters.getRightGridData().equalsIgnoreCase( "temperature" ) ) {
                        draw.drawGrid( mTemperature[ i ][ j ], 45, Parameters.getRightGridColour(), false, false, Parameters.getPixelSize(), i, j, mRightBufferedGraphics );
                    }
                    if( Parameters.getLeftGridData().equalsIgnoreCase( "temperature" ) ) {
                        draw.drawGrid( mTemperature[ i ][ j ], 45, Parameters.getLeftGridColour(), false, false, Parameters.getPixelSize(), i, j, mLeftBufferedGraphics );
                    } else if( Parameters.getLeftGridData().equalsIgnoreCase( "albedo" ) ) {
                        draw.drawGrid( mAlbedo[ i ][ j ], 1, Parameters.getLeftGridColour(), Parameters.isPlotLeftCoverage(), mCoverage[ i ][ j ], Parameters.getPixelSize(), i, j, mLeftBufferedGraphics );
                    }
                }
            }

            SwingUtilities.invokeLater( new Runnable() {
                @Override
                public void run() {

                    // Paint CA panels
                    mParent.mPanelRight.repaint();
                    mParent.mPanelLeft.repaint();
                }
            } );

            // If the thread has been told to stop, end the loop
            if( !mParent.isRunning() ) {
                break terminate;
            }
            // Draw a border around each off-screen grid image
            draw.drawBorder( mRightBufferedGraphics );
            draw.drawBorder( mLeftBufferedGraphics );

            // Draw the off screen images on the panel graphics component
            draw.drawImage( mParent.getOffScreenLeftGraphics(), mLeftBufferedImage );
            draw.drawImage( mParent.getOffScreenRightGraphics(), mRightBufferedImage );

            for( int i = 0; i < Parameters.getGridSize(); i++ ) {
                // Swap the old arrays with the new
                System.arraycopy( mNewTemperature[ i ], 0, mTemperature[ i ], 0, Parameters.getGridSize() );
                System.arraycopy( mNewAlbedo[ i ], 0, mAlbedo[ i ], 0, Parameters.getGridSize() );
                System.arraycopy( mNewCoverage[ i ], 0, mCoverage[ i ], 0, Parameters.getGridSize() );
            }
        }
        // End the thread when the insolation loop has finished
        mParent.stopCADaisyworld();
    }

    /**
     * Getter for property mHorizonalIndex.
     *
     * @return Value of property mHorizonalIndex.
     */
    public int getHorizonalIndex() {
        return mHorizonalIndex;
    }

    /**
     * Setter for property mHorizonalIndex.
     *
     * @param horizontalIndex New value of property mHorizonalIndex.
     */
    public void setHorizonalIndex( int horizontalIndex ) {
        mHorizonalIndex = horizontalIndex;
    }

    /**
     * Getter for property mVerticalIndex.
     *
     * @return Value of property mVerticalIndex.
     */
    public int getVerticalIndex() {
        return mVerticalIndex;
    }

    /**
     * Setter for property mVerticalIndex.
     *
     * @param verticalIndex New value of property mVerticalIndex.
     */
    public void setVerticalIndex( int verticalIndex ) {
        mVerticalIndex = verticalIndex;
    }
}
