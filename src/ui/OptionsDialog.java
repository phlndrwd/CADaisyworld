/*
 *   CADaisyworld - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Options.java
 *
 * <p>This class represents the options dialog from which all of the available
 * program options and model variables can be configured. The typical design
 * is also extremely flexible in that it ensures that any future advancements to
 * the main program can be reflected with relevant updates to the available
 * settings on the interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package ui;                             // Part of the UI package

import java.awt.Frame;                  // To handle constructor parent reference
import java.awt.event.WindowAdapter;    // For handling window events
import java.awt.event.WindowEvent;      // For generating window events
import javax.swing.JDialog;             // For extension of JDialog component
import javax.swing.JSpinner;            // The component used for input
import javax.swing.SpinnerModel;        // Defines the behaviour of a JSpinner
import javax.swing.SpinnerNumberModel;  // Defines numerical JSpinner behaviour
import model.Parameters;

/*
 * The class extends the JDialog component to make a custom dialog.
 */
public final class OptionsDialog extends JDialog {

    /*
     * The constructor for OptionsDialog.
     * @param parent A reference to the parent, CADaisyFrame.
     * @param modal Whether the dialog remains on top of other windows.
     */
    public OptionsDialog( Frame parent, boolean modal ) {
        super( parent, modal );
        initComponents();

        updateRightGridSettings();
        updateLeftGridSettings();

        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                cancel();
            }
        } );

        cancel();
    }

    /*
     * Controls user access to the tick box to plot coverage
     */
    private void updateRightGridSettings() {

        if( mComboRightGridData.getSelectedItem().toString().equalsIgnoreCase( "none" ) ) {
            mComboRightGridColour.setEnabled( false );
        } else {
            mComboRightGridColour.setEnabled( true );
        }

        if( mComboRightGridData.getSelectedItem().toString().equalsIgnoreCase( "albedo" ) ) {
            mCheckRightCoverage.setEnabled( true );
        } else {
            mCheckRightCoverage.setEnabled( false );
        }
        if( !mCheckRightCoverage.isEnabled() ) {
            mCheckRightCoverage.setSelected( false );
        }
    }

    /*
     * Controls user access to the tick box to plot coverage based on whether
     * albedo or anything else is to be plotted.
     */
    private void updateLeftGridSettings() {
        if( mComboLeftGridData.getSelectedItem().toString().equalsIgnoreCase( "none" ) ) {
            mComboLeftGridColour.setEnabled( false );
        } else {
            mComboLeftGridColour.setEnabled( true );
        }
        if( mComboLeftGridData.getSelectedItem().toString().equalsIgnoreCase( "albedo" ) ) {
            mCheckLeftCoverage.setEnabled( true );
        } else {
            mCheckLeftCoverage.setEnabled( false );
        }
        if( !mCheckLeftCoverage.isEnabled() ) {
            mCheckLeftCoverage.setSelected( false );
        }
    }

    /*
     * Saves the CA grid option settings
     */

    private void saveGridSettings() {
        int newGridSize = Integer.parseInt( mComboGridSize.getSelectedItem().toString().substring( 0, 3 ).replaceAll( "\\s", "" ) );
        Parameters.setGridSize( newGridSize );
        Parameters.resetPixelSize();
        Parameters.setLeftGridColour( mComboLeftGridColour.getSelectedItem().toString() );
        Parameters.setRightGridColour( mComboRightGridColour.getSelectedItem().toString() );
        Parameters.setLeftGridData( mComboLeftGridData.getSelectedItem().toString() );
        Parameters.setRightGridData( mComboRightGridData.getSelectedItem().toString() );
        Parameters.setPlotRightCoverage( mCheckRightCoverage.isSelected() );
        Parameters.setPlotLeftCoverage( mCheckLeftCoverage.isSelected() );
    }

    /*
     * Saves the model variables
     */
    private void saveModelSettings() {
        Parameters.setDeathRate( Double.parseDouble( mSpinnerDeathRate.getValue().toString() ) );
        Parameters.setThermalCapacity( Integer.parseInt( ( mSpinnerThermalCapacity.getValue().toString() ) ) );
        Parameters.setAlbedoGround( Double.parseDouble( ( mSpinnerAlbedoGround.getValue().toString() ) ) );
        Parameters.setDiffusionConstant( Integer.parseInt( mSpinnerDiffusionConstant.getValue().toString() ) );
        Parameters.setMutationRate( Double.parseDouble( mSpinnerMutationRate.getValue().toString() ) );
        Parameters.setInitialTemperature( Double.parseDouble( mSpinnerInitTemp.getValue().toString() ) );
        Parameters.setMaximumInsolation( Double.parseDouble( mSpinnerMaxInsolation.getValue().toString() ) );
        Parameters.setInsolationStep( Double.parseDouble( mSpinnerInsolationStep.getValue().toString() ) );
        Parameters.setVonNeumann( mRadioNeumann.isSelected() );
    }

    /*
     * Saves everthing
     */
    public void saveSettings() {
        saveGridSettings();
        saveModelSettings();
    }

    /*
     * Called if the window is closed or if 'cancel' is clicked, discards changes
     */
    public void cancel() {
        int comboIndex = 0;

        for( int index = 0; index < mComboGridSize.getItemCount(); index++ ) {
            String selectedGridSizeOption = ( String )mComboGridSize.getItemAt( index );
            selectedGridSizeOption = selectedGridSizeOption.substring( 0, 3 ).replaceAll( "\\s", "" );

            if( Integer.parseInt( selectedGridSizeOption ) == Parameters.getGridSize() ) {
                comboIndex = index;
                break;
            }
        }
        mComboGridSize.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboLeftGridData.getItemCount(); index++ ) {
            String selectedLeftDataOption = ( String )mComboLeftGridData.getItemAt( index );

            if( selectedLeftDataOption.equalsIgnoreCase( Parameters.getLeftGridData() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboLeftGridData.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboLeftGridColour.getItemCount(); index++ ) {
            String selectedLeftColourOption = ( String )mComboLeftGridColour.getItemAt( index );

            if( selectedLeftColourOption.equalsIgnoreCase( Parameters.getLeftGridColour() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboLeftGridColour.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboRightGridData.getItemCount(); index++ ) {
            String selectedRightDataOption = ( String )mComboRightGridData.getItemAt( index );

            if( selectedRightDataOption.equalsIgnoreCase( Parameters.getRightGridData() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboRightGridData.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboRightGridColour.getItemCount(); index++ ) {
            String selectedRightColourOption = ( String )mComboRightGridColour.getItemAt( index );

            if( selectedRightColourOption.equalsIgnoreCase( Parameters.getRightGridColour() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboRightGridColour.setSelectedIndex( comboIndex );

        mCheckLeftCoverage.setSelected( Parameters.isPlotLeftCoverage() );
        mCheckRightCoverage.setSelected( Parameters.isPlotRightCoverage() );

        if( Parameters.isVonNeumann() == true ) {
            mRadioNeumann.setSelected( true );
        } else {
            mRadioMoore.setSelected( true );
        }

        mSpinnerAlbedoGround.setValue( Parameters.getAlbedoGround() );
        mSpinnerDeathRate.setValue( Parameters.getDeathRate() );
        mSpinnerDiffusionConstant.setValue( Parameters.getDiffusionConstant() );
        mSpinnerInitTemp.setValue( Parameters.getInitialTemperature() );
        mSpinnerInsolationStep.setValue( Parameters.getInsolationStep() );
        mSpinnerMaxInsolation.setValue( Parameters.getMaximumInsolation() );
        mSpinnerMutationRate.setValue( Parameters.getMutationRate() );
        mSpinnerThermalCapacity.setValue( Parameters.getThermalCapacity() );

        setVisible( false );
    }

    /*
     * Called if 'OK' is clicked and saves settings.
     */
    public void ok() {
        setVisible( false );
        saveSettings();
    }

    /*
     * Called if 'Apply' is clicked, saves settings but keeps dialog visible
     */
    public void apply() {
        saveSettings();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupNeighborhood = new javax.swing.ButtonGroup();
        mPanelGridOptions = new javax.swing.JPanel();
        mLabelGridSize = new javax.swing.JLabel();
        mComboGridSize = new javax.swing.JComboBox<>();
        mPanelLeftGrid = new javax.swing.JPanel();
        mLabelLeftGridData = new javax.swing.JLabel();
        mComboLeftGridData = new javax.swing.JComboBox<>();
        mLabelLeftGridColour = new javax.swing.JLabel();
        mComboLeftGridColour = new javax.swing.JComboBox<>();
        mCheckLeftCoverage = new javax.swing.JCheckBox();
        mPanelRightGrid = new javax.swing.JPanel();
        mLabelRightGridData = new javax.swing.JLabel();
        mComboRightGridData = new javax.swing.JComboBox<>();
        mLabelRightGridColour = new javax.swing.JLabel();
        mComboRightGridColour = new javax.swing.JComboBox<>();
        mCheckRightCoverage = new javax.swing.JCheckBox();
        mPanelModelBehaviour = new javax.swing.JPanel();
        mPanelInsolation = new javax.swing.JPanel();
        mLabelMaxInsolation = new javax.swing.JLabel();
        SpinnerModel maxInsolationModel = new SpinnerNumberModel(3, 0.0, 5, 0.1);
        mSpinnerMaxInsolation = new JSpinner(maxInsolationModel);
        mLabelInsolationStep = new javax.swing.JLabel();
        SpinnerModel insolationStepModel = new SpinnerNumberModel(0.001, 0.001, 0.1, 0.001);
        mSpinnerInsolationStep = new JSpinner(insolationStepModel);
        mPanelNeighborhood = new javax.swing.JPanel();
        mRadioNeumann = new javax.swing.JRadioButton();
        mRadioMoore = new javax.swing.JRadioButton();
        mPanelModelVariables = new javax.swing.JPanel();
        mLabelDiffusionConstant = new javax.swing.JLabel();
        SpinnerModel diffusionModel = new SpinnerNumberModel(500, 0, 1000, 1);
        mSpinnerDiffusionConstant = new JSpinner(diffusionModel);
        mLabelThermalCapacity = new javax.swing.JLabel();
        SpinnerModel capacityModel = new SpinnerNumberModel(2500, 0, 5000, 1);
        mSpinnerThermalCapacity = new JSpinner(capacityModel);
        mLabelAlbedoGround = new javax.swing.JLabel();
        SpinnerModel albedoModel = new SpinnerNumberModel(0.5, 0, 1, 0.01);
        mSpinnerAlbedoGround = new JSpinner(albedoModel);
        mLabelDeathRate = new javax.swing.JLabel();
        SpinnerModel deathModel = new SpinnerNumberModel(0.02, 0.00, 1.00, 0.01);
        mSpinnerDeathRate = new JSpinner(deathModel);
        mLabelMutationRate = new javax.swing.JLabel();
        SpinnerModel mutationModel = new SpinnerNumberModel(0.02, 0.00, 0.5, 0.001);
        mSpinnerMutationRate = new JSpinner(mutationModel);
        mLabelInitTemp = new javax.swing.JLabel();
        SpinnerModel initTempModel = new SpinnerNumberModel(22.5, 0.0, 40.0, 0.1);
        mSpinnerInitTemp = new JSpinner(initTempModel);
        mButtonApply = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonOK = new javax.swing.JButton();

        groupNeighborhood.add(mRadioNeumann);
        groupNeighborhood.add(mRadioMoore);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        mPanelGridOptions.setBorder(javax.swing.BorderFactory.createTitledBorder("Grid Options"));

        mLabelGridSize.setText("Size:");

        mComboGridSize.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "10 x 10", "20 x 20", "50 x 50", "80 x 80", "100 x 100", "200 x 200", "400 x 400" }));
        mComboGridSize.setSelectedIndex( 1 );
        mComboGridSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mComboGridSizeActionPerformed(evt);
            }
        });

        mPanelLeftGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Left Grid"));

        mLabelLeftGridData.setText("Data:");

        mComboLeftGridData.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Temperature", "Albedo", "None" }));
        mComboLeftGridData.setSelectedIndex( 0 );
        mComboLeftGridData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mComboLeftGridDataActionPerformed(evt);
            }
        });

        mLabelLeftGridColour.setText("Colour:");

        mComboLeftGridColour.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Jet", "Plasma", "Lava", "Greenscale", "Greyscale" }));
        mComboLeftGridColour.setSelectedIndex( 0 );

        mCheckLeftCoverage.setText("Plot Coverage");
        mCheckLeftCoverage.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mCheckLeftCoverage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCheckLeftCoverageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mPanelLeftGridLayout = new javax.swing.GroupLayout(mPanelLeftGrid);
        mPanelLeftGrid.setLayout(mPanelLeftGridLayout);
        mPanelLeftGridLayout.setHorizontalGroup(
            mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelLeftGridLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mCheckLeftCoverage, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
                    .addGroup(mPanelLeftGridLayout.createSequentialGroup()
                        .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mLabelLeftGridData)
                            .addComponent(mLabelLeftGridColour))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mComboLeftGridData, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mComboLeftGridColour, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        mPanelLeftGridLayout.setVerticalGroup(
            mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelLeftGridLayout.createSequentialGroup()
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelLeftGridData)
                    .addComponent(mComboLeftGridData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelLeftGridColour)
                    .addComponent(mComboLeftGridColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mCheckLeftCoverage)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelRightGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Right Grid"));

        mLabelRightGridData.setText("Data:");

        mComboRightGridData.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Temperature", "Albedo", "None" }));
        mComboRightGridData.setSelectedIndex( 1 );
        mComboRightGridData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mComboRightGridDataActionPerformed(evt);
            }
        });

        mLabelRightGridColour.setText("Colour:");

        mComboRightGridColour.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Jet", "Plasma", "Lava", "Greenscale", "Greyscale" }));
        mComboRightGridColour.setSelectedIndex( 4 );

        mCheckRightCoverage.setSelected(true);
        mCheckRightCoverage.setText("Plot Coverage");
        mCheckRightCoverage.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelRightGridLayout = new javax.swing.GroupLayout(mPanelRightGrid);
        mPanelRightGrid.setLayout(mPanelRightGridLayout);
        mPanelRightGridLayout.setHorizontalGroup(
            mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelRightGridLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mCheckRightCoverage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(mPanelRightGridLayout.createSequentialGroup()
                        .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mLabelRightGridData)
                            .addComponent(mLabelRightGridColour))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mComboRightGridColour, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mComboRightGridData, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        mPanelRightGridLayout.setVerticalGroup(
            mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelRightGridLayout.createSequentialGroup()
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelRightGridData)
                    .addComponent(mComboRightGridData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelRightGridColour)
                    .addComponent(mComboRightGridColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mCheckRightCoverage)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout mPanelGridOptionsLayout = new javax.swing.GroupLayout(mPanelGridOptions);
        mPanelGridOptions.setLayout(mPanelGridOptionsLayout);
        mPanelGridOptionsLayout.setHorizontalGroup(
            mPanelGridOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelGridOptionsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelGridOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelGridOptionsLayout.createSequentialGroup()
                        .addComponent(mPanelLeftGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mPanelRightGrid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mPanelGridOptionsLayout.createSequentialGroup()
                        .addComponent(mLabelGridSize)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mComboGridSize, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        mPanelGridOptionsLayout.setVerticalGroup(
            mPanelGridOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelGridOptionsLayout.createSequentialGroup()
                .addGroup(mPanelGridOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelGridSize)
                    .addComponent(mComboGridSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelGridOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelRightGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelLeftGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelModelBehaviour.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Behaviour"));

        mPanelInsolation.setBorder(javax.swing.BorderFactory.createTitledBorder("Insolation Loop"));

        mLabelMaxInsolation.setText("Max. Insolation ( S ):");

        mLabelInsolationStep.setText("Step Size ( ΔS ):");

        javax.swing.GroupLayout mPanelInsolationLayout = new javax.swing.GroupLayout(mPanelInsolation);
        mPanelInsolation.setLayout(mPanelInsolationLayout);
        mPanelInsolationLayout.setHorizontalGroup(
            mPanelInsolationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelInsolationLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mLabelMaxInsolation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mSpinnerMaxInsolation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mLabelInsolationStep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mSpinnerInsolationStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        mPanelInsolationLayout.setVerticalGroup(
            mPanelInsolationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelInsolationLayout.createSequentialGroup()
                .addGroup(mPanelInsolationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelMaxInsolation)
                    .addComponent(mSpinnerMaxInsolation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelInsolationStep)
                    .addComponent(mSpinnerInsolationStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelNeighborhood.setBorder(javax.swing.BorderFactory.createTitledBorder("Cell Neighborhood"));

        mRadioNeumann.setText("von Neumann");
        mRadioNeumann.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioMoore.setSelected(true);
        mRadioMoore.setText("Moore");
        mRadioMoore.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelNeighborhoodLayout = new javax.swing.GroupLayout(mPanelNeighborhood);
        mPanelNeighborhood.setLayout(mPanelNeighborhoodLayout);
        mPanelNeighborhoodLayout.setHorizontalGroup(
            mPanelNeighborhoodLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelNeighborhoodLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mRadioNeumann)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mRadioMoore)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mPanelNeighborhoodLayout.setVerticalGroup(
            mPanelNeighborhoodLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelNeighborhoodLayout.createSequentialGroup()
                .addGroup(mPanelNeighborhoodLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mRadioMoore)
                    .addComponent(mRadioNeumann))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout mPanelModelBehaviourLayout = new javax.swing.GroupLayout(mPanelModelBehaviour);
        mPanelModelBehaviour.setLayout(mPanelModelBehaviourLayout);
        mPanelModelBehaviourLayout.setHorizontalGroup(
            mPanelModelBehaviourLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelModelBehaviourLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelModelBehaviourLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelInsolation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mPanelNeighborhood, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mPanelModelBehaviourLayout.setVerticalGroup(
            mPanelModelBehaviourLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelModelBehaviourLayout.createSequentialGroup()
                .addComponent(mPanelInsolation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mPanelNeighborhood, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelModelVariables.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Variables"));

        mLabelDiffusionConstant.setText("<html>Diffusion Constant ( D<sub>T</sub> ):</html>");

        mLabelThermalCapacity.setText("Thermal Capacity ( C ):");

        mLabelAlbedoGround.setText("<html>Albedo Ground ( A<sub>0</sub> ):</html>");

        mLabelDeathRate.setText("Death Rate ( γ ):");

        mLabelMutationRate.setText("Mutation Rate ( r ):");

        mLabelInitTemp.setText("Init. Temp. ( °C ):");

        javax.swing.GroupLayout mPanelModelVariablesLayout = new javax.swing.GroupLayout(mPanelModelVariables);
        mPanelModelVariables.setLayout(mPanelModelVariablesLayout);
        mPanelModelVariablesLayout.setHorizontalGroup(
            mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mPanelModelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mLabelMutationRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelAlbedoGround)
                    .addComponent(mLabelDiffusionConstant))
                .addGap(5, 5, 5)
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mSpinnerMutationRate)
                    .addComponent(mSpinnerAlbedoGround)
                    .addComponent(mSpinnerDiffusionConstant))
                .addGap(15, 15, 15)
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mLabelThermalCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                    .addComponent(mLabelDeathRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelInitTemp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mSpinnerInitTemp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                    .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mSpinnerThermalCapacity, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(28, 28, 28))
        );
        mPanelModelVariablesLayout.setVerticalGroup(
            mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelModelVariablesLayout.createSequentialGroup()
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerDiffusionConstant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelThermalCapacity)
                    .addComponent(mSpinnerThermalCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelDiffusionConstant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerAlbedoGround, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelDeathRate)
                    .addComponent(mLabelAlbedoGround, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelModelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerMutationRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerInitTemp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelInitTemp)
                    .addComponent(mLabelMutationRate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(mButtonOK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply))
                    .addComponent(mPanelGridOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mPanelModelBehaviour, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mPanelModelVariables, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mPanelGridOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mPanelModelBehaviour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mPanelModelVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mButtonOK)
                    .addComponent(mButtonCancel)
                    .addComponent(mButtonApply))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mCheckLeftCoverageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCheckLeftCoverageActionPerformed
    }//GEN-LAST:event_mCheckLeftCoverageActionPerformed

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    private void mComboRightGridDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mComboRightGridDataActionPerformed
        updateRightGridSettings();
    }//GEN-LAST:event_mComboRightGridDataActionPerformed

    private void mComboLeftGridDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mComboLeftGridDataActionPerformed
        updateLeftGridSettings();
    }//GEN-LAST:event_mComboLeftGridDataActionPerformed

    private void mComboGridSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mComboGridSizeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mComboGridSizeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                new OptionsDialog( new javax.swing.JFrame(), true ).setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup groupNeighborhood;
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JCheckBox mCheckLeftCoverage;
    private javax.swing.JCheckBox mCheckRightCoverage;
    private javax.swing.JComboBox<String> mComboGridSize;
    private javax.swing.JComboBox<String> mComboLeftGridColour;
    private javax.swing.JComboBox<String> mComboLeftGridData;
    private javax.swing.JComboBox<String> mComboRightGridColour;
    private javax.swing.JComboBox<String> mComboRightGridData;
    private javax.swing.JLabel mLabelAlbedoGround;
    private javax.swing.JLabel mLabelDeathRate;
    private javax.swing.JLabel mLabelDiffusionConstant;
    private javax.swing.JLabel mLabelGridSize;
    private javax.swing.JLabel mLabelInitTemp;
    private javax.swing.JLabel mLabelInsolationStep;
    private javax.swing.JLabel mLabelLeftGridColour;
    private javax.swing.JLabel mLabelLeftGridData;
    private javax.swing.JLabel mLabelMaxInsolation;
    private javax.swing.JLabel mLabelMutationRate;
    private javax.swing.JLabel mLabelRightGridColour;
    private javax.swing.JLabel mLabelRightGridData;
    private javax.swing.JLabel mLabelThermalCapacity;
    private javax.swing.JPanel mPanelGridOptions;
    private javax.swing.JPanel mPanelInsolation;
    private javax.swing.JPanel mPanelLeftGrid;
    private javax.swing.JPanel mPanelModelBehaviour;
    private javax.swing.JPanel mPanelModelVariables;
    private javax.swing.JPanel mPanelNeighborhood;
    private javax.swing.JPanel mPanelRightGrid;
    private javax.swing.JRadioButton mRadioMoore;
    private javax.swing.JRadioButton mRadioNeumann;
    private javax.swing.JSpinner mSpinnerAlbedoGround;
    private javax.swing.JSpinner mSpinnerDeathRate;
    private javax.swing.JSpinner mSpinnerDiffusionConstant;
    private javax.swing.JSpinner mSpinnerInitTemp;
    private javax.swing.JSpinner mSpinnerInsolationStep;
    private javax.swing.JSpinner mSpinnerMaxInsolation;
    private javax.swing.JSpinner mSpinnerMutationRate;
    private javax.swing.JSpinner mSpinnerThermalCapacity;
    // End of variables declaration//GEN-END:variables
}
