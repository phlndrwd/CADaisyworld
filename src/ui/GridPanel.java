/* 
 *   CADaisyworld - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GridPanel.java
 *
 * <p>This class extends JPanel so that the two Cellular Automaton panels of the
 * main interface can instantiate this one and so have the benefit of their
 * paintComponent method being overridden. This enables the BufferedImage,
 * passed as a parameter to be drawn on the panel from the thread loop, avoiding
 * the need for semaphore controlled image creation.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package ui;                  // Part of the UserInterface package

import java.awt.Graphics;               // Used to handle graphics components
import java.awt.image.BufferedImage;    // To handle the passed image
import javax.swing.JPanel;              // The component this class extends

/*
 * The class extends the JPanel component to customise paint behaviour.
 */
public class GridPanel extends JPanel {

    BufferedImage mBufferedImage;

    /*
     * Constructs new panel with BufferedImage as a parameter.
     * @bi The BufferedImage
     */
    public GridPanel( BufferedImage bufferedImage ) {
        mBufferedImage = bufferedImage;
    }

    /*
     * An override of the standard JPanel method, draws the image passed
     * as a parameter on the JPanel itself.
     */
    @Override
    protected void paintComponent( Graphics graphics ) {
        super.paintComponent( graphics );
        graphics.drawImage( mBufferedImage, 0, 0, null );
    }
}
