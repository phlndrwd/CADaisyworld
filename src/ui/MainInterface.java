/*
 *   CADaisyworld - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MainInterface.java
 *
 * <p>This is the main class of a two-dimensional Daisyworld example that 
 * attempts to replicate as faithfully as possible the equations of von Bloh 
 * et al. (1997), see MainThread.java for the model logic. The model 
 * variables are configurable from a seperate JDialog which also includes 
 * additional program detail and colour options, see Options.java for this.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package ui;                             // Part of the UI package

import model.Parameters;
import draw.ValueToColour;
import java.awt.Graphics;               // For drawing
import java.awt.Color;                  // For creating and defining colours
import java.awt.event.WindowAdapter;    // For handling window events
import java.awt.event.WindowEvent;      // For generating window events
import java.awt.image.BufferedImage;
import javax.swing.JFrame;              // For extension of JFrame component
import model.MainThread;

/*
 * The class extends the JFrame component in order to provide a robust GUI
 */
public final class MainInterface extends JFrame {

    private static BufferedImage mOffScreenLeftImage;
    private static BufferedImage mOffScreenRightImage;

    private static Graphics mOffScreenLeftGraphics;
    private static Graphics mOffScreenRightGraphics;

    private boolean mIsThreadRunning; // Thread stop and start control flag

    private Thread mThread;

    private final AboutDialog mAboutDialog;
    private final OptionsDialog mOptionsDialog;

    /*
     * Constructor to initialize the GUI components and centre window on screen
     */
    public MainInterface() {

        mOffScreenLeftImage = new BufferedImage( Parameters.getImageSize() + 1, Parameters.getImageSize() + 1, BufferedImage.TYPE_INT_RGB );
        mOffScreenRightImage = new BufferedImage( Parameters.getImageSize() + 1, Parameters.getImageSize() + 1, BufferedImage.TYPE_INT_RGB );

        mOffScreenLeftGraphics = mOffScreenLeftImage.getGraphics();
        mOffScreenRightGraphics = mOffScreenRightImage.getGraphics();

        mAboutDialog = new AboutDialog( this, true );
        mOptionsDialog = new OptionsDialog( this, true );

        initComponents();

        stopCADaisyworld();

        // When the window is closed, ensure the thread is closed gracefully
        addWindowListener(
                new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                stopCADaisyworld();
            }
        }
        );
    }

    /*
     * Draws the legend according to colour scheme and data to be plotted.
     * @param g The graphics component of either of the legend JPanels.
     * @colour The name of the scheme to use.
     */
    private void initLegend( Graphics graphics, String colour, String data ) {

        Color c;                                // Colour object reference
        ValueToColour vc = new ValueToColour(); // To convert numbers to colours

        graphics.clearRect( 0, 0, Parameters.getImageSize() + 1, 50 );           // Clear old legend

        if( !data.equalsIgnoreCase( "none" ) ) {   // If plotting is required

            for( int i = 0; i < Parameters.getImageSize(); i++ ) {

                // Set chosen colour
                if( colour.equalsIgnoreCase( "greyscale" ) ) {
                    c = vc.toGreyscale( i, 0, Parameters.getImageSize() );
                } else if( colour.equalsIgnoreCase( "greenscale" ) ) {
                    c = vc.toGreenscale( i, 0, Parameters.getImageSize() );
                } else if( colour.equalsIgnoreCase( "lava" ) ) {
                    c = vc.toLava( i, 0, Parameters.getImageSize() );
                } else if( colour.equalsIgnoreCase( "plasma" ) ) {
                    c = vc.toPlasma( i, 0, Parameters.getImageSize() );
                } else {
                    c = vc.toJet( i, 0, Parameters.getImageSize() );
                }
                // Draw a colour representation of the point in the loop
                graphics.setColor( c );
                graphics.fillRect( i + 1, 1, i + 1, 25 );
            }
            // Draw a black border and the number zero at the left
            c = new Color( 0, 0, 0 );
            graphics.setColor( c );
            graphics.drawRect( 0, 0, Parameters.getImageSize() + 1, 25 );
            graphics.drawString( "0", 0, 40 );

            // Draw the appropriate data label and the maximum on the right
            if( data.equalsIgnoreCase( "temperature" ) ) {
                graphics.drawString( "45", 388, 40 );
                graphics.drawString( "Temperature \u00b0C", 158, 40 );
            } else if( data.equalsIgnoreCase( "albedo" ) ) {
                graphics.drawString( "1", 397, 40 );
                graphics.drawString( "Albedo", 183, 40 );
            }
        }
    }

    /*
     * The method to start mRunning the cellular automaton, called from clicking
     * the start button.
     */
    private void startCADaisyworld() {

        // Draw right legend
        initLegend( mPanelRightLegend.getGraphics(), Parameters.getRightGridColour(), Parameters.getRightGridData() );
        // Draw left legend
        initLegend( mPanelLeftLegend.getGraphics(), Parameters.getLeftGridColour(), Parameters.getLeftGridData() );

        mMenuStart.setEnabled( false );
        mMenuStop.setEnabled( true );
        mMenuOptions.setEnabled( false );

        setRunning( true );                    // CA is mRunning

        /*
         * Instantiate a new thread requiring a runnable object at a parameter,
         * create a new instance of MainThread with appropriate parameters
         * taken from the options dialog, a reference to this class as a parent
         * and the two graphics objects to the off screen images.
         */
        mThread = new Thread( new MainThread( this ) );
        mThread.start();
    }

    /*
     * The method to stop mRunning the cellular automaton, called from clicking
     * the stop button or when the insolation loop of the thread completes.
     */
    public void stopCADaisyworld() {
        mMenuStart.setEnabled( true );
        mMenuStop.setEnabled( false );
        mMenuOptions.setEnabled( true );

        setRunning( false );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mPanelLeft = new GridPanel(mOffScreenLeftImage);
        mPanelRight = new GridPanel(mOffScreenRightImage);
        mPanelRightLegend = new javax.swing.JPanel();
        mPanelLeftLegend = new javax.swing.JPanel();
        mMenuBar = new javax.swing.JMenuBar();
        mMenuFile = new javax.swing.JMenu();
        mMenuAbout = new javax.swing.JMenuItem();
        mFileMenuMenuSeperator = new javax.swing.JPopupMenu.Separator();
        mMenuExit = new javax.swing.JMenuItem();
        mMenuEdit = new javax.swing.JMenu();
        mMenuOptions = new javax.swing.JMenuItem();
        mMenuRun = new javax.swing.JMenu();
        mMenuStart = new javax.swing.JMenuItem();
        mMenuStop = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CADaisyworld");
        setName(""); // NOI18N
        setResizable(false);

        mPanelLeft.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        mPanelLeft.setDoubleBuffered(false);
        mPanelLeft.setPreferredSize(new java.awt.Dimension(402, 402));

        javax.swing.GroupLayout mPanelLeftLayout = new javax.swing.GroupLayout(mPanelLeft);
        mPanelLeft.setLayout(mPanelLeftLayout);
        mPanelLeftLayout.setHorizontalGroup(
            mPanelLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        mPanelLeftLayout.setVerticalGroup(
            mPanelLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        mPanelRight.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        mPanelRight.setDoubleBuffered(false);
        mPanelRight.setPreferredSize(new java.awt.Dimension(402, 402));

        javax.swing.GroupLayout mPanelRightLayout = new javax.swing.GroupLayout(mPanelRight);
        mPanelRight.setLayout(mPanelRightLayout);
        mPanelRightLayout.setHorizontalGroup(
            mPanelRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        mPanelRightLayout.setVerticalGroup(
            mPanelRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        mPanelRightLegend.setPreferredSize(new java.awt.Dimension(402, 50));

        javax.swing.GroupLayout mPanelRightLegendLayout = new javax.swing.GroupLayout(mPanelRightLegend);
        mPanelRightLegend.setLayout(mPanelRightLegendLayout);
        mPanelRightLegendLayout.setHorizontalGroup(
            mPanelRightLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 402, Short.MAX_VALUE)
        );
        mPanelRightLegendLayout.setVerticalGroup(
            mPanelRightLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        mPanelLeftLegend.setPreferredSize(new java.awt.Dimension(402, 50));

        javax.swing.GroupLayout mPanelLeftLegendLayout = new javax.swing.GroupLayout(mPanelLeftLegend);
        mPanelLeftLegend.setLayout(mPanelLeftLegendLayout);
        mPanelLeftLegendLayout.setHorizontalGroup(
            mPanelLeftLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 402, Short.MAX_VALUE)
        );
        mPanelLeftLegendLayout.setVerticalGroup(
            mPanelLeftLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        mMenuFile.setText("File");

        mMenuAbout.setText("About");
        mMenuAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuAboutActionPerformed(evt);
            }
        });
        mMenuFile.add(mMenuAbout);
        mMenuFile.add(mFileMenuMenuSeperator);

        mMenuExit.setText("Exit");
        mMenuFile.add(mMenuExit);

        mMenuBar.add(mMenuFile);

        mMenuEdit.setText("Edit");

        mMenuOptions.setText("Options");
        mMenuOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuOptionsActionPerformed(evt);
            }
        });
        mMenuEdit.add(mMenuOptions);

        mMenuBar.add(mMenuEdit);

        mMenuRun.setText("Run");

        mMenuStart.setText("Start");
        mMenuStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuStartActionPerformed(evt);
            }
        });
        mMenuRun.add(mMenuStart);

        mMenuStop.setText("Stop");
        mMenuStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuStopActionPerformed(evt);
            }
        });
        mMenuRun.add(mMenuStop);

        mMenuBar.add(mMenuRun);

        setJMenuBar(mMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelLeftLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelRightLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mPanelRightLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelLeftLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mMenuOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuOptionsActionPerformed
        Parameters.centreWindow( mOptionsDialog );
        mOptionsDialog.setVisible( true );
    }//GEN-LAST:event_mMenuOptionsActionPerformed

    private void mMenuAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuAboutActionPerformed
        Parameters.centreWindow( mAboutDialog );
        mAboutDialog.setVisible( true );
    }//GEN-LAST:event_mMenuAboutActionPerformed

    private void mMenuStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuStopActionPerformed
        stopCADaisyworld();
    }//GEN-LAST:event_mMenuStopActionPerformed

    private void mMenuStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuStartActionPerformed
        startCADaisyworld();
    }//GEN-LAST:event_mMenuStartActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                MainInterface mainInterface = new MainInterface();
                Parameters.centreWindow( mainInterface );
                mainInterface.setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPopupMenu.Separator mFileMenuMenuSeperator;
    private javax.swing.JMenuItem mMenuAbout;
    private javax.swing.JMenuBar mMenuBar;
    private javax.swing.JMenu mMenuEdit;
    private javax.swing.JMenuItem mMenuExit;
    private javax.swing.JMenu mMenuFile;
    private javax.swing.JMenuItem mMenuOptions;
    private javax.swing.JMenu mMenuRun;
    private javax.swing.JMenuItem mMenuStart;
    private javax.swing.JMenuItem mMenuStop;
    public javax.swing.JPanel mPanelLeft;
    private javax.swing.JPanel mPanelLeftLegend;
    public javax.swing.JPanel mPanelRight;
    private javax.swing.JPanel mPanelRightLegend;
    // End of variables declaration//GEN-END:variables

    /**
     * Getter for property mRunning.
     *
     * @return Value of property mRunning.
     */
    public boolean isRunning() {
        return mIsThreadRunning;
    }

    /**
     * Setter for property mRunning.
     *
     * @param running New value of property mRunning.
     */
    public void setRunning( boolean running ) {
        mIsThreadRunning = running;
    }

    public Graphics getOffScreenLeftGraphics() {
        return mOffScreenLeftGraphics;
    }

    public Graphics getOffScreenRightGraphics() {
        return mOffScreenRightGraphics;
    }
}
