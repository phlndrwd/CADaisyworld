/*
 *   CADaisyworld - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ValueToColour.java
 *
 * <p>This class has methods to turn a value within a specified range into a
 * colour within the chosen spectrum. It's designed as a generic class that can
 * be used with any program that may wish to have such functionality. In the
 * context of the two-dimensional Daisyworld it turns values representing
 * temperature and albedo into appropriate colours so that that they can be
 * drawn on the graphical cellular automaton grid.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package draw;                // Part of the drawing package

import java.awt.Color;          // For creating and defining colours

public class ValueToColour {

    /*
     * Turn a value to a colour within the range that most closely resembles
     * Matlab's 'jet' colour range. This is the default for displaying
     * temperature.
     * @param value The value to turn into a colour.
     * @param valueLow The lowest value within the range.
     * @param valueHigh The highest value within the range.
     * @return The newly derived colour.
     */
    public Color toJet( double value, double valueLow, double valueHigh ) {

        Color c;             // The unintialised colour variable

        // Clamp the value to one within the range
        if( value < valueLow ) {
            value = valueLow;
        }
        if( value > valueHigh ) {
            value = valueHigh;
        }
        double range = valueHigh - valueLow;    // Calculate the range
        double multiplier = 1275 / range;       // Calculate multiplier
        // Translate value to one within colour range
        double valueCol = value * multiplier;

        if( valueCol <= 0 ) {                    // Defensive programming
            c = new Color( 0, 0, 128 );           // Dark blue
        } else if( ( valueCol > 0 ) && ( valueCol < 128 ) ) {
            c = new Color( 0, 0, 128 + ( int )valueCol );   // Dark blue to blue
        } else if( ( valueCol >= 128 ) && ( valueCol < 383 ) ) {
            valueCol -= 128;
            c = new Color( 0, ( int )valueCol, 255 ); // Blue to cyan
        } else if( ( valueCol >= 383 ) && ( valueCol < 638 ) ) {
            valueCol -= 383;
            c = new Color( 0, 255, 255 - ( int )valueCol ); // Cyan to green
        } else if( ( valueCol >= 638 ) && ( valueCol < 893 ) ) {
            valueCol -= 638;
            c = new Color( ( int )valueCol, 255, 0 );   // Green to yellow
        } else if( ( valueCol >= 893 ) && ( valueCol < 1148 ) ) {
            valueCol -= 893;
            c = new Color( 255, 255 - ( int )valueCol, 0 ); // Yellow to red
        } else if( ( valueCol >= 1148 ) && ( valueCol < 1276 ) ) {
            valueCol -= 1148;
            c = new Color( 255 - ( int )valueCol, 0, 0 ); // Red to dark red
        } else {
            c = new Color( 128, 0, 0 );           // Default to dark red
        }

        return c;
    }

    /*
     * Turns a value to a colour within a range named plasma because the red/
     * blue/cyan/white spectrum is reminiscent of a plasma lamp. Most
     * appropriate for displaying temperature.
     * @param value The value to turn into a colour.
     * @param valueLow The lowest value within the range.
     * @param valueHigh The highest value within the range.
     * @return The newly derived colour.
     */
    public Color toPlasma( double value, double valueLow, double valueHigh ) {

        Color c;             // The unintialised colour variable

        // Clamp the value to one within the range
        if( value < valueLow ) {
            value = valueLow;
        }
        if( value > valueHigh ) {
            value = valueHigh;
        }
        double range = valueHigh - valueLow;    // Calculate the range
        double multiplier = 1020 / range;       // Calculate multiplier
        // Translate value to one within colour range
        double valueCol = value * multiplier;

        if( valueCol <= 0 ) {                    // Defensive programming
            c = new Color( 255, 255, 255 );           // White
        } else if( ( valueCol > 0 ) && ( valueCol < 255 ) ) {
            c = new Color( 255 - ( int )valueCol, 255, 255 );       // White to cyan
        } else if( ( valueCol >= 255 ) && ( valueCol < 510 ) ) {
            valueCol -= 255;
            c = new Color( 0, 255 - ( int )valueCol, 255 ); // Cyan to blue
        } else if( ( valueCol >= 510 ) && ( valueCol < 765 ) ) {
            valueCol -= 510;
            c = new Color( ( int )valueCol, 0, 255 );       // Blue to magenta
        } else if( ( valueCol >= 765 ) && ( valueCol < 1020 ) ) {
            valueCol -= 765;
            c = new Color( 255, 0, 255 - ( int )valueCol );     // Magenta to red
        } else {
            c = new Color( 255, 0, 0 );       // Default to red
        }

        return c;
    }

    /*
     * Turns a value to a colour within a range named lava because the black/
     * red/orange/white spectrum is reminiscent of the colours seen in volcanic
     * lava.
     * @param value The value to turn into a colour.
     * @param valueLow The lowest value within the range.
     * @param valueHigh The highest value within the range.
     * @return The newly derived colour.
     */
    public Color toLava( double value, double valueLow, double valueHigh ) {

        Color c;             // The unintialised colour variable

        // Clamp the value to one within the range
        if( value < valueLow ) {
            value = valueLow;
        }
        if( value > valueHigh ) {
            value = valueHigh;
        }
        double range = valueHigh - valueLow;    // Calculate the range
        double multiplier = 765 / range;        // Calculate multiplier
        // Translate value to one within colour range
        double valueCol = value * multiplier;

        if( valueCol <= 0 ) {                    // Defensive programming
            c = new Color( 0, 0, 0 );             // Pure black
        } else if( ( valueCol > 0 ) && ( valueCol < 255 ) ) {
            c = new Color( ( int )valueCol, 0, 0 );       // Black to red
        } else if( ( valueCol >= 255 ) && ( valueCol < 510 ) ) {
            valueCol -= 255;
            c = new Color( 255, ( int )valueCol, 0 );     // Red to yellow
        } else if( ( valueCol >= 510 ) && ( valueCol < 765 ) ) {
            valueCol -= 510;
            c = new Color( 255, 255, ( int )valueCol );  // Yellow to white
        } else {
            c = new Color( 255, 255, 255 );       // Default to white
        }

        return c;
    }

    /*
     * Turns a value to a colour within a lower range of black to green. Most
     * appropriate for displaying albedo.
     * @param value The value to turn into a colour.
     * @param valueLow The lowest value within the range.
     * @param valueHigh The highest value within the range.
     * @return The newly derived colour.
     */
    public Color toGreenscale( double value, double valueLow, double valueHigh ) {

        Color c;             // The unintialised colour variable

        // Clamp the value to one within the range
        if( value < valueLow ) {
            value = valueLow;
        }
        if( value > valueHigh ) {
            value = valueHigh;
        }
        double range = valueHigh - valueLow;    // Calculate the range
        double multiplier = 255 / range;        // Calculate multiplier
        // Translate value to one within colour range
        double valueCol = value * multiplier;

        if( valueCol <= 0 ) {                    // Defensive programming
            c = new Color( 0, 0, 0 );             // Pure black
        } else if( ( valueCol > 0 ) && ( valueCol < 255 ) ) {
            c = new Color( 0, ( int )valueCol, 0 );     // Black to green
        } else {
            c = new Color( 0, 255, 0 );           // Default to green
        }

        return c;
    }

    /*
     * Turns a value to a colour within the greyscale range of black to white.
     * This is the default for displaying albedo.
     * @param value The value to turn into a colour.
     * @param valueLow The lowest value within the range.
     * @param valueHigh The highest value within the range.
     * @return The newly derived colour.
     */
    public Color toGreyscale( double value, double valueLow, double valueHigh ) {

        Color c;             // The unintialised colour variable

        // Clamp the value to one within the range
        if( value < valueLow ) {
            value = valueLow;
        }
        if( value > valueHigh ) {
            value = valueHigh;
        }
        double range = valueHigh - valueLow;    // Calculate the range
        double valueCol = value / range;        // Calculate multiplier

        // A colour within the greyscale spectrum can be defined simply by
        // stepping the values of red, green and blue together.
        c = new Color( ( float )valueCol, ( float )valueCol, ( float )valueCol );

        return c;
    }
}
