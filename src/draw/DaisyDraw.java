/*
 *   CADaisyworld - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * DaisyDraw.java
 *
 * <p>This class was a later addition to the system after it was deemed logical
 * to remove the need for the main model to handle any of the drawing routines
 * itself. By instantiating this class from the main loop all of the necessary
 * drawing routines can be performed by this class alone.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package draw;                               // Part of the drawing package

import java.awt.Color;                      // To define new colours
import java.awt.Graphics;                   // To handle graphics components
import java.awt.image.BufferedImage;        // To draw on offscreen images
import model.Parameters;

public class DaisyDraw {

    Color mColour = null; // A reference to a colour object for use with methods

    /*
     * Draws a square representing the passed value within a chosen colour
     * spectrum on the passed Graphics component.
     * @param value The value to be translated into a colour.
     * @param max The maximum expected value within the range.
     * @param colour A string representation of the user selected colour scheme.
     * @param plotCoverage Whether coverage should be plotted, only with albedo.
     * @param coverage Whether there is coverage on the site being looked at.
     * @param pixelSize The size of the CA pixel given the user configuration.
     * @param i The horiztonal index within the array.
     * @param j The vertical index within the array.
     * @param g The graphics component to draw on.
     */
    public void drawGrid( double value, int max, String colour, boolean plotCoverage, boolean coverage, int pixelSize, int i, int j, Graphics g ) {

        // Used to convert numbers into corresponding colours within a spectrum
        ValueToColour vc = new ValueToColour();

        // Derive colour using ValueToColour
        if( colour.equalsIgnoreCase( "greyscale" ) ) {
            mColour = vc.toGreyscale( value, 0, max );
        } else if( colour.equalsIgnoreCase( "greenscale" ) ) {
            mColour = vc.toGreenscale( value, 0, max );
        } else if( colour.equalsIgnoreCase( "lava" ) ) {
            mColour = vc.toLava( value, 0, max );
        } else if( colour.equalsIgnoreCase( "plasma" ) ) {
            mColour = vc.toPlasma( value, 0, max );
        } else if( colour.equalsIgnoreCase( "jet" ) ) {
            mColour = vc.toJet( value, 0, max );
        }

        if( plotCoverage ) {         // If coverage should be plotted...
            if( !coverage ) {        // then alter colour where there is no daisy
                if( colour.equalsIgnoreCase( "greyscale" ) ) {
                    mColour = new Color( 255, 0, 0 );
                } else if( colour.equalsIgnoreCase( "greenscale" ) ) {
                    mColour = new Color( 255, 255, 0 );
                } else if( colour.equalsIgnoreCase( "lava" ) ) {
                    mColour = new Color( 0, 0, 255 );
                } else if( colour.equalsIgnoreCase( "plasma" ) ) {
                    mColour = new Color( 0, 0, 0 );
                } else {
                    mColour = new Color( 255, 255, 255 );
                }
            }
        }
        // Set the colour and draw a square of the correct size.
        g.setColor( mColour );
        g.fillRect( ( i * pixelSize ) + 1, ( j * pixelSize ) + 1, ( i * pixelSize ) + ( pixelSize + 1 ), ( j * pixelSize ) + ( pixelSize + 1 ) );
    }

    /*
     * A basic function that is called at the end of each cycle to add a black
     * border around each of the drawn CA images.
     */
    public void drawBorder( Graphics g ) {
        // Draw a border around each off-screen grid image
        mColour = new Color( 0, 0, 0 );
        g.setColor( mColour );
        g.drawRect( 0, 0, Parameters.getImageSize() + 1, Parameters.getImageSize() + 1 );
    }

    /*
     * This function draws the completed offscreen image onto the graphics
     * component of the given panel on the main interface.
     * @param image The graphics component of the panel.
     * @param offscreen The offscreen image drawn.
     */
    public void drawImage( Graphics image, BufferedImage offscreen ) {
        image.drawImage( offscreen, 0, 0, null );
    }
}
